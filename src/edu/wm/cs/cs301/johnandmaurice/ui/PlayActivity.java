package edu.wm.cs.cs301.johnandmaurice.ui;
import edu.wm.cs.cs301.johnandmaurice.falstad.Robot;
import edu.wm.cs.cs301.johnandmaurice.R;
import edu.wm.cs.cs301.johnandmaurice.R.id;
import edu.wm.cs.cs301.johnandmaurice.R.layout;
import edu.wm.cs.cs301.johnandmaurice.R.menu;
import edu.wm.cs.cs301.johnandmaurice.falstad.*;
import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Direction;
import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Turn;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;


public class PlayActivity extends Activity {

	private boolean isManual;
	MazePanel v;
	public Maze maze;
	private SharedPreferences settings;
	private String driverSelected;
	private String fileSelected;
	private Robot robot;
	private RobotDriver driver;
	private float initBattery = 2500;
	private ProgressBar batteryBar;
	
	final Handler dHandler = new Handler();
	Direction nextmove = null;
    protected Runnable dUpdateResults = new Runnable() {
        public void run() {
            try {
				pressLeft(null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    };

	/**
	 * sets up the elements on the screen when the activity starts
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play);
		
		maze = ((Maze)getApplicationContext());
		
		settings = this.getSharedPreferences("selections", MODE_PRIVATE);

		driverSelected = settings.getString("driver", "");
		fileSelected = settings.getString("file", "");
		
		v = new MazePanel(this);
		v = (MazePanel)findViewById(R.id.MazePanel);
		
		maze.setMazePanel(v);
		
		this.robot = new BasicRobot(initBattery, true, true, true, false, true, false);
		
		Log.v("playactivity", driverSelected);
		
		if (driverSelected == "Manual Driver") {
			this.driver = new ManualDriver();
		} else if (driverSelected == "Gambler") {
			this.driver = new Gambler();
		} else if (driverSelected == "Wall Follower") {
			this.driver = new WallFollower();
		} else if (driverSelected == "Wizard") {
			this.driver = new Wizard();
		} else if (driverSelected == "Tremaux") {
			this.driver = new Tremaux();
		} else {
			this.driver = new ManualDriver();
		}
			
		this.robot.setMaze(maze);
		
		try {
			this.driver.setRobot(this.robot);
		} catch (UnsuitableRobotException e) {
			//e.printStackTrace();
		}
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		isManual = (boolean) getIntent().getBooleanExtra("isManual", true);
		if (isManual == true) {
			//
		} else {
			findViewById(R.id.dpad).setVisibility(View.INVISIBLE);
			findViewById(R.id.autoButtons).setVisibility(View.VISIBLE);
		}
		
		batteryBar = (ProgressBar)findViewById(R.id.batteryBar);
		batteryBar.setMax((int)initBattery);
		batteryBar.setProgress((int)initBattery);
		
		this.robot.getMaze().notifyViewerRedraw();
	}

	/**
	 * sets up the options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.play, menu);
		return true;
	}

	/**
	 * handles action bar interaction
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_maze) {
			Log.v("PlayActivity", "Toggle Map");
			int key;
			key = 'm';
			this.driver.getRobot().getMaze().keyDown(key);
			this.driver.getRobot().getMaze().getPanel().invalidate();
			return true;
		} else if (id == R.id.action_solution) {
			Log.v("PlayActivity", "Toggle Solution");	
			int key;
			key = 's';
			this.driver.getRobot().getMaze().keyDown(key);
			this.driver.getRobot().getMaze().getPanel().invalidate();
			return true;
		} else if (id == R.id.action_walls) {
			int key;
			key = 'z';
			this.driver.getRobot().getMaze().keyDown(key);
			this.driver.getRobot().getMaze().getPanel().invalidate();
			Log.v("PlayActivity", "Toggle Walls");	
			return true;
		} else if (id == R.id.action_plus) {
			int key;
			key = '+';
			this.driver.getRobot().getMaze().keyDown(key);
			this.driver.getRobot().getMaze().getPanel().invalidate();
			Log.v("PlayActivity", "Increase Map Scale");	
			return true;
		} else if (id == R.id.action_minus) {
			int key;
			key = '-';
			this.driver.getRobot().getMaze().keyDown(key);
			this.driver.getRobot().getMaze().getPanel().invalidate();
			Log.v("PlayActivity", "Decrease Map Scale");	
			return true;
		} else if (id == android.R.id.home) {
			Intent intent = new Intent(this, AMazeActivity.class);
			Log.v("PlayActivity", "Press Back");
			startActivity(intent);
			finish();
			
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Creates an intent with an extra, which goes to the Finish activity. The extra "goal" signals 
	 * whether the robot reached the goal or not. This method is used if the robot reaches the goal.
	 * @param v not used
	 */
	public void gotoFinishGoal(View v) {
		Intent intent = new Intent(this, FinishActivity.class);
		intent.putExtra("goal", true);
		intent.putExtra("cell", this.robot.getCellCount());
		intent.putExtra("battery", this.initBattery - this.robot.getBatteryLevel());
		Log.v("PlayActivity", "Press Goal");
		
		startActivity(intent);
		finish();
	}

	/**
	 * Creates an intent with an extra, which goes to the Finish activity. The extra "goal" signals 
	 * whether the robot reached the goal or not. This method is used if the robot runs out of battery.
	 * @param v not used
	 */
	public void gotoFinishDead(View v) {
		Intent intent = new Intent(this, FinishActivity.class);
		intent.putExtra("goal", false);
		intent.putExtra("cell", this.robot.getCellCount());
		intent.putExtra("battery", this.initBattery - this.robot.getBatteryLevel());
		Log.v("PlayActivity", "Press Dead");
		startActivity(intent);
		finish();
	}

	/**
	 * logs button presses
	 * @param v unused
	 */
	public void pressLeft(View v) {
		Log.v("PlayActivity", "Press Left");
		if (this.driver.getRobot().getBatteryLevel() < 0) {
			gotoFinishDead(null);
		}
		try {
			this.driver.getRobot().rotate(Robot.Turn.LEFT);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		};
		batteryBar.setProgress(batteryBar.getProgress() - 3);
		this.driver.getRobot().getMaze().getPanel().postInvalidate();
	}

	/**
	 * logs button presses
	 * @param v unused
	 */
	public void pressUp(View v) {
		Log.v("PlayActivity", "Press Up");
		if (this.driver.getRobot().getBatteryLevel() < 0) {
			gotoFinishDead(null);
		}
		try {
			this.driver.getRobot().move(1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		};
		if (this.driver.getRobot().isSteppingOut()) {
			gotoFinishGoal(null);
		}
		batteryBar.setProgress(batteryBar.getProgress() - (int)this.robot.getEnergyForStepForward());
		this.driver.getRobot().getMaze().getPanel().postInvalidate();	
		}

	/**
	 * logs button presses
	 * @param v unused
	 */
	public void pressDown(View v) {
		Log.v("PlayActivity", "Press Down");
		if (this.driver.getRobot().getBatteryLevel() < 0) {
			gotoFinishDead(null);
		}
		try {
			this.driver.getRobot().rotate(Robot.Turn.AROUND);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		};
		batteryBar.setProgress(batteryBar.getProgress() - 6);
		this.driver.getRobot().getMaze().getPanel().postInvalidate();	
	}

	/**
	 * logs button presses
	 * @param v unused
	 */
	public void pressRight(View v) {
		Log.v("PlayActivity", "Press Right");
		if (this.driver.getRobot().getBatteryLevel() < 0) {
			gotoFinishDead(null);
		}
		try {
			this.driver.getRobot().rotate(Robot.Turn.RIGHT);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		};
		batteryBar.setProgress(batteryBar.getProgress() - 3);
		this.driver.getRobot().getMaze().getPanel().postInvalidate();	
	}

	/**
	 * logs button presses
	 * @param v unused
	 */
	


	public void pressStart(View v) {
		Log.v("PlayActivity", "Press Start");
		//final RobotDriver workingDriver = this.driver;
		/*Thread driveThread = new Thread(new Runnable() {
			//RobotDriver driver = PlayActivity.this.driver;
			
			public void run() {
				
				while (!driver.getRobot().isSteppingOut()) {
					
					try {
						nextmove = driver.drive2Exit();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//final Direction nextDirection = nextmove;
					
					dHandler.postDelayed(new Runnable() {
						//Direction nextmove = nextDirection;
						public void run() {
							try {
								rotateAndMove(nextmove);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}, 4000);
					
					//pressUp(null);
				}
			}
		});
		
		driveThread.start();*/
		
		
		   Thread t = new Thread() {
	            public void run() {
	            	
	            	while(!PlayActivity.this.driver.getRobot().isAtGoal()){
	                try {
	                	PlayActivity.this.nextmove = PlayActivity.this.driver.drive2Exit();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	                
	                if (dHandler.postDelayed(dUpdateResults, 3000) == false){
	                	Log.v("PlayActivity", "hander not working");
	                }
	            }
	        
	           }
		   };
	        
		   t.start();
	       
	}

	/**
	 * logs button presses
	 * @param v unused
	 */
	public void pressPause(View v) {
		Log.v("PlayActivity", "Press Pause");
	}
	
	public void rotateAndMove(Direction chosenDirection) throws Exception {
		if (chosenDirection == Direction.FORWARD){
			pressUp(null);
		} else if (chosenDirection == Direction.LEFT) {
			pressLeft(null);
		} else if (chosenDirection == Direction.RIGHT) {
			pressRight(null);
		} else {
			pressDown(null);
		}
		
		
		
	}
}