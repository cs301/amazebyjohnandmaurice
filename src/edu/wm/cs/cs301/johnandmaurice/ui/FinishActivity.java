package edu.wm.cs.cs301.johnandmaurice.ui;

import edu.wm.cs.cs301.johnandmaurice.R;
import edu.wm.cs.cs301.johnandmaurice.R.id;
import edu.wm.cs.cs301.johnandmaurice.R.layout;
import edu.wm.cs.cs301.johnandmaurice.R.menu;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class FinishActivity extends Activity {

	private TextView titleView;
	private int cellCount;
	private float battery;
	
	private SharedPreferences settings;

	/**
	 * sets up the elements on the screen when the activity starts
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_finish);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		cellCount = (int) getIntent().getIntExtra("cell", 0);
		
		TextView cellView = (TextView) findViewById(R.id.textView4);
		cellView.setText("" + cellCount);
		
		battery = (float) getIntent().getFloatExtra("battery", 0);
		
		TextView batteryView = (TextView) findViewById(R.id.textView5);
		batteryView.setText("" + battery);

		boolean reachGoal = (boolean) getIntent().getBooleanExtra("goal", true);
		titleView = (TextView) findViewById(R.id.finishTitle);

		if (reachGoal == true) {
			//Toast.makeText(getApplicationContext(), "Reached goal", Toast.LENGTH_SHORT).show();
			titleView.setText(R.string.win_string);			
		} else {
			//Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
			titleView.setText(R.string.fail_message);
		}
	}

	/**
	 * sets up the options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finish, menu);
		return true;
	}

	/**
	 * handles action bar interaction
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
//			Intent intent = new Intent(this, AMazeActivity.class);
//			Log.v("FinishActivity", "Press Back");
//			startActivity(intent);
			finish();
		}
		//		if (id == R.id.action_settings) {
		//			return true;
		//		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Creates an intent, which goes to the Title activity.
	 * @param v not used
	 */
	public void gotoTitle(View v) {
		Intent intent = new Intent(this, AMazeActivity.class);
		Log.v("FinishActivity", "Press Return to Title");
		startActivity(intent);
	
		finish();
	}
}
