package edu.wm.cs.cs301.johnandmaurice.ui;

import java.io.File;
import java.util.ArrayList;
import edu.wm.cs.cs301.johnandmaurice.R;
import edu.wm.cs.cs301.johnandmaurice.R.id;
import edu.wm.cs.cs301.johnandmaurice.R.layout;
import edu.wm.cs.cs301.johnandmaurice.R.menu;
import edu.wm.cs.cs301.johnandmaurice.falstad.MazePanel;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.INotificationSideChannel.Stub;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AMazeActivity extends Activity {

	private SeekBar seekBar;
	private TextView skillView;
	private TextView fileInstructions;
	private Spinner fileSelect;
	private Spinner SaveMazeSpinner;
	protected int skillSelected;
	protected String driverSelected;
	protected String algorithmSelected;
	protected String fileSelected;
	protected boolean SaveMaze;
	protected boolean loadMaze;
	
	private boolean isManual;
	
	public MazePanel panel;
	public TextView savePrompt;
	
	File currentDir;

	public static final String ALGORITHM="pref_alg";
	public static final String SKILL="pref_skill";
	public static final String DRIVER="pref_driver";
	public static final String FILE="pref_file";
	

	private SharedPreferences settings;

	/**
	 * sets up the elements on the screen when the activity starts
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_amaze);

		//getActionBar().setDisplayHomeAsUpEnabled(true);
		
		
		currentDir = getFilesDir();
		
		createSaveMazeSpinner();
		createFileSelectSpinner();	
		createAlgorithmSpinner();
		createDriverSpinner();
		createSeekbar();		
	}

	/**
	 * sets up the options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.amaze, menu);
		return true;
	}

	/**
	 * handles action bar interaction
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		//		if (id == R.id.action_settings) {
		//			return true;
		//		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Creates an intent with an extra, which goes to the Generating activity. The extra "isManual" signals 
	 * whether we will use a manual driver or not to the next activity.
	 * @param v not used
	 */
	public void gotoGenerating(View v) {

		settings = this.getSharedPreferences("selections", MODE_PRIVATE);
		Editor edit = settings.edit();
		edit.clear();
		edit.putString("algorithm", algorithmSelected);
		edit.putInt("skill", skillSelected);
		edit.putString("driver", driverSelected);
		edit.putString("file", fileSelected);
		edit.commit();
		//Toast.makeText(this, "details are saved..", Toast.LENGTH_LONG).show();

		Intent intent = new Intent(this, GeneratingActivity.class);
		intent.putExtra("isManual", isManual);
		intent.putExtra("saveMaze", SaveMaze);
		intent.putExtra("loadMaze", loadMaze);
		startActivity(intent);
		finish();
	}

	/**
	 * helper method to create the skill seekbar for display
	 */
	private void createSeekbar() {
		// Seekbar
		seekBar = (SeekBar) findViewById(R.id.seekBar1);
		skillView = (TextView) findViewById(R.id.textView5);
		// Initialize the skillView with '0'
		skillView.setText("Skill Selected: " + seekBar.getProgress() + "/" + seekBar.getMax());
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			int progress = 0;
			@Override
			public void onProgressChanged(SeekBar seekBar, 
					int progresValue, boolean fromUser) {
				progress = progresValue;
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// Display the value in skillView
				skillView.setText("Skill Selected: " + progress + "/" + seekBar.getMax());
				skillSelected = progress;
				Log.v("AMazeActivity", "Selected Skill: " + String.valueOf(skillSelected));
				//Toast.makeText(getApplicationContext(), "Selected Skill: " + skillSelected, Toast.LENGTH_SHORT).show();
			}
		});
	}

	/**
	 * helper method to create the driver spinner for display
	 */
	private void createDriverSpinner() {
		// Driver spinner
		Spinner spinner2 = initSpinner(R.id.spinner2, R.array.drivers);
		spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapter, View v,
					int position, long id) {
				String item = adapter.getItemAtPosition(position).toString();
				// log selected spinner item
				Log.v("AMazeActivity", "Selected Driver: " + String.valueOf(item));
				//Toast.makeText(getApplicationContext(), "Selected Driver: " + item, Toast.LENGTH_SHORT).show();
				driverSelected = item;
				if (item.equals("Manual Driver")) {
					isManual = true;
				} else {
					isManual = false;
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	/**
	 * helper method to create the driver spinner for display
	 */
	private void createSaveMazeSpinner() {
		// Save Maze spinner
		SaveMazeSpinner = initSpinner(R.id.saveMazeSpinner, R.array.SaveMazeQuestion);
		SaveMazeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapter, View v,
					int position, long id) {
				String item = adapter.getItemAtPosition(position).toString();
				// log selected spinner item
				Log.v("AMazeActivity", "Selected Driver: " + String.valueOf(item));
				//Toast.makeText(getApplicationContext(), "Selected Driver: " + item, Toast.LENGTH_SHORT).show();
				 
			
				if (item.equals("yes")) {
					SaveMaze = true;
				} else {
					SaveMaze = false;
				}
				
				Log.v("SaveMaze", "" + SaveMaze);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}
	
	/**
	 * helper method to create the algorithm spinner for display
	 */
	private void createAlgorithmSpinner() {
		// Algorithm spinner
		Spinner spinner = initSpinner(R.id.spinner1, R.array.algorithms);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapter, View v,
					int position, long id) {
				String item = adapter.getItemAtPosition(position).toString();
				// log selected spinner item
				Log.v("AMazeActivity", "Selected Algorithm: " + String.valueOf(item));
				//Toast.makeText(getApplicationContext(), "Selected Algorithm: " + item, Toast.LENGTH_SHORT).show();
				if (item.equals("Load From File")) {
					seekBar.setVisibility(View.INVISIBLE);
					skillView.setVisibility(View.INVISIBLE);
					SaveMazeSpinner.setVisibility(View.INVISIBLE);
					fileInstructions.setVisibility(View.VISIBLE);
					fileSelect.setVisibility(View.VISIBLE);
					savePrompt.setVisibility(View.INVISIBLE);
				} else {
					fileSelect.setVisibility(View.INVISIBLE);
					fileInstructions.setVisibility(View.INVISIBLE);
					SaveMazeSpinner.setVisibility(View.VISIBLE);
					seekBar.setVisibility(View.VISIBLE);
					skillView.setVisibility(View.VISIBLE);
					savePrompt.setVisibility(View.VISIBLE);
				}
				algorithmSelected = item;
				
				if(item.equals("Load From File")){
					loadMaze = true;
				}
				else{ loadMaze = false;}
				
				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
	}

	/**
	 * helper method to create the file select for display
	 */
	private void createFileSelectSpinner() {
		// File select spinner
		
		
		fileInstructions = (TextView) findViewById(R.id.fileInstructions);
		fileInstructions.setVisibility(View.INVISIBLE);
		savePrompt = (TextView) findViewById(R.id.savePrompt);
		fileSelect = initSpinner(R.id.spinner3, StringifyDir(currentDir));
		fileSelect.setVisibility(View.INVISIBLE);
		fileSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapter, View v,
					int position, long id) {
				String item = adapter.getItemAtPosition(position).toString();
				// log selected spinner item
				Log.v("AMazeActivity", "Selected File: " + String.valueOf(item));
				//Toast.makeText(getApplicationContext(), "Selected File: " + item, Toast.LENGTH_SHORT).show();
				fileSelected = item;
				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
	}
	
	private ArrayList<String> StringifyDir(File dir){
		File[] directoryListing = dir.listFiles();
		
		if (directoryListing.length == 0){
			ArrayList<String> oops = new ArrayList<String>();
			oops.add( "Sorry No Maze Files Created Yet");
			return oops; 
		}
		ArrayList<String> filenames = new ArrayList<String>();
		 for (int i = 0; i < directoryListing.length; i++) {
		  // Do something with child
		filenames.add(directoryListing[i].toString().replace("/data/data/edu.wm.cs.cs301.johnandmaurice/files/", ""));
		}
		
		return filenames;
		
	}
	private Spinner initSpinner(int viewid, ArrayList<String> dropList ) {
		Spinner spinner = (Spinner) findViewById(viewid);
		// Create an ArrayAdapter
		ArrayAdapter<String> fileAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,dropList );
		// Specify the layout

		// Apply the adapter
		spinner.setAdapter(fileAdapter);
		return spinner;
	}

	/**
	 * helper method to set up a generic spinner for display
	 * @param viewid the id of the spinner to be initialized
	 * @param arrayid the id of the array with which to populate the spinner
	 * @return the initialized spinner
	 */
	private Spinner initSpinner(int viewid, int arrayid) {
		Spinner spinner = (Spinner) findViewById(viewid);
		// Create an ArrayAdapter
		ArrayAdapter<CharSequence> fileAdapter = ArrayAdapter.createFromResource(this,
				arrayid, android.R.layout.simple_spinner_item);
		// Specify the layout
		fileAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter
		spinner.setAdapter(fileAdapter);
		return spinner;
	}
}