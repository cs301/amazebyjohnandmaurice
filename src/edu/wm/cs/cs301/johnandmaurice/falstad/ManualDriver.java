package edu.wm.cs.cs301.johnandmaurice.falstad;

import java.util.ArrayList;
import java.util.List;

import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Direction;
import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Turn;

/**
 * 
 * 
 * @author John Sawin
 * @author Maurice Hayward
 */
public class ManualDriver implements RobotDriver {
	
	private Robot robot;
	int pathLength;
	private int width;
	private int height;
	private Distance distance;
	private float initialEnergy;
	public int cellCounter;
	protected SingleRandom random; // random number stream, used to make randomized decisions, e.g for direction to go
	public List<Direction> possibleMoves = new ArrayList<Direction>();
	public float initBattery = 2500;
	
	public ManualDriver() {
		random = SingleRandom.getRandom();
	}

	/**
	 * Assigns a robot platform to the driver. Not all robot configurations may be suitable such that the method 
	 * will throw an exception if the robot does not match minimal configuration requirements, e.g. providing a sensor
	 * to measure the distance to an object in a particular direction. 
	 * @param r robot to operate
	 * @throws UnsuitableRobotException if driver cannot operate the given robot
	 */
	@Override
	public void setRobot(Robot r) throws UnsuitableRobotException {
		this.robot = r;
		this.initialEnergy = this.robot.getBatteryLevel();
	}

	/**
	 * Provides the robot driver with information on the dimensions of the 2D maze
	 * measured in the number of cells in each direction.
	 * Only some drivers such as Tremaux's algorithm need this information.
	 * @param width of the maze
	 * @param height of the maze
	 * @precondition 0 <= width, 0 <= height of the maze.
	 */
	@Override
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;		
	}

	/**
	 * Provides the robot driver with information on the distance to the exit.
	 * Only some drivers such as the wizard rely on this information to find the exit.
	 * @param distance gives the length of path from current position to the exit.
	 * @precondition null != distance, a full functional distance object for the current maze.
	 */
	@Override
	public void setDistance(Distance distance) {
		this.distance = distance;
	}

	/**
	 * Drives the robot towards the exit given it exists and given the robot's energy supply lasts long enough. 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws exception if robot stopped due to some problem, e.g. lack of energy
	 */
	@Override
	public Direction drive2Exit() throws Exception {
		//this.getRobot().getMaze().MazeView.driverType = 1;
		if (this.getRobot().getBatteryLevel() <= 0) {
			this.getRobot().getMaze().state = Constants.STATE_DEAD;
			return null;
		} else {
			return null;
		}
	}

	/**
	 * Returns the total energy consumption of the journey, i.e.,
	 * the difference between the robot's initial energy level at
	 * the starting position and its energy level at the exit position. 
	 * This is used as a measure of efficiency for a robot driver.
	 */
	@Override
	public float getEnergyConsumption() {
		//System.out.print(this.initialEnergy + " - " + this.robot.getBatteryLevel() + " = ");
		return this.initialEnergy - this.robot.getBatteryLevel();
	}

	/**
	 * Returns the total length of the journey in number of cells traversed. 
	 * Being at the initial position counts as 0. 
	 * This is used as a measure of efficiency for a robot driver.
	 */
	@Override
	public int getPathLength() {
		return this.robot.getCellCount();
	}
	
	/**
	 * Returns current robot
	 * @return the current robot object of the driver
	 */
	@Override
	public Robot getRobot() {
		return this.robot;
	}
	
	/**
	 * Adds to the current cellCounter
	 * @param count the amount to increment cellCounter by
	 */

	

	
	/**
	 * Checks all possible moves and gathers the ones available
	 */
	public void gatherAvailableMoves() {
		possibleMoves.clear();
		if (isOnStaightPath()){possibleMoves.add(Direction.FORWARD);
		
		}
		
		else if ( this.robot.isAtJunction()){
			
			if(!HitWall()){possibleMoves.add(Direction.FORWARD);}
			
			if(!CheckIsWallOnRight()){possibleMoves.add(Direction.RIGHT);}
			
			if(!WallOnLeft()){possibleMoves.add(Direction.LEFT);}
		}
		
		else if( isAtDeadEnd()){possibleMoves.add(Direction.BACKWARD);}
	}
		
	/**
	 * Rotates and moves the robot based on the chosen direction
	 * @param chosenDirection the direction the robot wants to move in
	 * @throws Exception if cannot move
	 */
	public void rotateAndMove(Direction chosenDirection) throws Exception {
		if (chosenDirection == Direction.FORWARD){
			//
		} else if (chosenDirection == Direction.LEFT) {
			this.getRobot().rotate(Turn.LEFT);
		} else if (chosenDirection == Direction.RIGHT) {
			this.getRobot().rotate(Turn.RIGHT);
		} else {
			this.getRobot().rotate(Turn.AROUND);
		}
		
		this.getRobot().move(1);
	}
	
	/**
	 * Check to see if a wall is on the right side of the robot, without a right sensor
	 * @return true if there is a wall, false if otherwise
	 */
	public boolean CheckIsWallOnRight()  {
		try {
			this.getRobot().rotate(Turn.RIGHT);
		} catch (Exception e) {
			return false;
		}
		boolean Wall = false;
		if(HitWall()){ Wall = true;}
		else{ Wall = false;}
		try {
			this.getRobot().rotate(Turn.LEFT);
		} catch (Exception e) {
			//e.printStackTrace();
			return false;
		}
		return Wall;
	}
	public boolean isOnStaightPath(){
		return !isAtDeadEnd() && !this.robot.isAtJunction();
	}
	
	/**
	 * Check to see if a wall is on the left side of the robot, uses left sensor
	 * @return true if there is a wall, false if otherwise
	 */
	public boolean WallOnLeft(){
		return this.robot.distanceToObstacle(Direction.LEFT)==0;
	}
	
	/**
	 * Check to see if a wall is on the right side of the robot, uses right sensor
	 * @return true if there is a wall, false if otherwise
	 */
	public boolean WallOnRight(){
		return this.robot.distanceToObstacle(Direction.RIGHT)==0;
	}
	
	/**
	 * checks to see if a wall is directly in front of the robot, uses front sensor
	 * @return true if wall in front, false otherwise
	 */
	public boolean HitWall(){
		return this.robot.distanceToObstacle(Direction.FORWARD) == 0;
	}
	
	/**
	 * Determines if the robot is at a dead end
	 * @return true if there is no junction, and a wall is in front of the robot
	 */
	public boolean isAtDeadEnd(){
		return !this.robot.isAtJunction() && HitWall();
	}
	
	/**
	 * Picks a random direction to move from all possible moves
	 * @return the random direction
	 */
	public Direction pickRandomDirection() {
		int length = possibleMoves.size();
		int rand = random.nextIntWithinInterval(0, length - 1);
		
		return possibleMoves.get(rand);
	}
	
	/**
	 * Steps out of the maze
	 */
	public void stepOut() throws Exception {
		while (!(this.getRobot().distanceToObstacle(Direction.FORWARD) > 40)) {
			this.getRobot().rotate(Turn.LEFT);
		}
		this.getRobot().move(1);
	}
	
	
	
	/**
	 * Test function used for debugging
	 */
	public void TestEverything(){
		System.out.println(" is atDeatEND: " + isAtDeadEnd());
		System.out.println(" is sraightpath: " + isOnStaightPath());
		System.out.println(" is Junction: " + this.getRobot().isAtJunction());
		System.out.println("possibleMoves:  " + possibleMoves.toString());
		System.out.println("WALL IN FRONT:  " + HitWall());
		System.out.println("WALL ON LEFT :" + WallOnLeft());
		System.out.println("WALL ON RIght :" + WallOnRight());
		
		System.out.println(""
				+ ""
				+ "");
	}



	
}