package edu.wm.cs.cs301.johnandmaurice.falstad;

import java.util.ArrayList;

import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Cardinal;
import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Direction;
import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Turn;

//TODO: Fix Tremaux


public class Tremaux extends ManualDriver {
	
	ArrayList<ArrayList<Integer>>CellVisted = new ArrayList<ArrayList<Integer>>();
	ArrayList<ArrayList<Integer>>CurrentPath = new ArrayList<ArrayList<Integer>>();
	Robot.Direction NextMove = null;
	
public Tremaux() {		Robot r = new BasicRobot(2500, true, true, true, false, true, false);
	try {
		this.setRobot(r);
} catch (UnsuitableRobotException e) {
		e.printStackTrace();
	}
}

@Override
public Direction drive2Exit() throws Exception {	
	
		ArrayList<Integer> current_pos = getPositionList(this.getRobot().getCurrentPosition());
		
		if(isOnStaightPath()){
			if(!CurrentPath.contains(current_pos)){
				CurrentPath.add(current_pos);
				CellVisted.add(current_pos);
				//rotateAndMove(Direction.FORWARD);
				NextMove = Direction.FORWARD;
			}
		}
		else if(this.getRobot().isInsideRoom()){
			while(this.getRobot().isInsideRoom()){
				current_pos = getPositionList(this.getRobot().getCurrentPosition());
			//Direction NextMove;
			if(!WallOnLeft()){
				//rotateAndMove(Direction.LEFT);
				NextMove = Direction.LEFT;
			}
			
			current_pos = getPositionList(this.getRobot().getCurrentPosition());
			gatherAvailableMoves();
			if(possibleMoves.contains(Direction.LEFT)){NextMove = Direction.LEFT;}
			else{NextMove = possibleMoves.get(0);}
			CurrentPath.add(current_pos);
			CellVisted.add(current_pos);
			//rotateAndMove(NextMove);
			NextMove = Direction.LEFT;
				
			}
		}
		else if(this.getRobot().isAtJunction()){
			//Direction nextMove;
			
			if(!CellVisted.contains(current_pos)){
				CurrentPath.add(current_pos);
				CellVisted.add(current_pos);
				gatherAvailableMoves();
				NextMove = pickRandomDirection();
				//rotateAndMove(nextMove);

			}
			else{backTrack();
			
				
			}
		}
		else if (isAtDeadEnd()){
			if(CurrentPath.size()==0){
				NextMove = Direction.FORWARD;
			}
			else{
			backTrack();}
			
		}
		
	
	
	if (this.getRobot().isAtGoal() == true) {
		stepOut();
	}
	

	return NextMove ;
}
private void backTrack() throws Exception{
	this.getRobot().rotate(Turn.AROUND);
	this.getRobot().move(1);
	
	while(true){
		ArrayList<Integer> current_pos = getPositionList(this.getRobot().getCurrentPosition());
		if(this.getRobot().isAtJunction()||this.getRobot().isInsideRoom()){
			gatherAvailableMoves();
			for ( Direction Move : possibleMoves ){
				if(CurrentPath.contains(Move)){
					possibleMoves.remove(Move);
				}
			}
			if(!possibleMoves.isEmpty()){
				rotateAndMove(pickRandomDirection());
				break;
			}
			else{ gatherAvailableMoves();
			CurrentPath.remove(current_pos);
			rotateAndMove(pickRandomDirection());
			}
			
		}
		else{
			CurrentPath.remove(current_pos);
			rotateAndMove(Direction.FORWARD);
			
			
		}
	}
}

private ArrayList<Integer> getPositionList( int[] postionArry){
	
	ArrayList<Integer> pos = new ArrayList<Integer>();
	pos.add(postionArry[0]);
	pos.add(postionArry[1]);
	
	return pos;

}
public ArrayList<Integer> getAdjDirection(Direction direction){
	
	int[] currentPosition;
	try {
		currentPosition = this.getRobot().getCurrentPosition();
	} catch (Exception e) {
		throw new UnsupportedOperationException("Position is out of bounds");
	}
	int[] AdjPosition = new int[2]; 
	
	int x = currentPosition[0];
	int y = currentPosition[1];
	
	Cardinal AbsDirection = this.getRobot().relativeToCardinal(direction);
	switch(AbsDirection){
	case EAST: AdjPosition[0] = x + Constants.EAST[0];
				AdjPosition[1] = y + Constants.EAST[1];
		break;
	case NORTH:AdjPosition[0] = x + Constants.NORTH[0];
	AdjPosition[1] = y + Constants.NORTH[1];
		break;
	case SOUTH:AdjPosition[0] = x + Constants.SOUTH[0];
	AdjPosition[1] = y + Constants.SOUTH[1];
		break;
	case WEST:AdjPosition[0] = x + Constants.WEST[0];
	AdjPosition[1] = y + Constants.WEST[1];
		break;
	}
	
	return getPositionList(AdjPosition);
}
}
