package edu.wm.cs.cs301.johnandmaurice.falstad;

import java.util.*;

import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Direction;
import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Turn;

/**
 * A simple random search algorithm with no memory. Once started, it 
 * 1) checks its options by asking its available distance sensors, 
 * 2) randomly picks a direction, 
 * 3) rotates/moves a step 
 * and then repeats with step 1 unless it finds the exit.
 * 
 * @author John Sawin
 * @author Maurice Hayward
 */
public class Gambler extends ManualDriver {

	/**
	 * Default constructor for the Gambler class, using the standard robot configuration
	 */
	public Gambler() {
		Robot r = new BasicRobot(2500, true, true, true, false, true, false);
		try {
			this.setRobot(r);
		} catch (UnsuitableRobotException e) {
			//e.printStackTrace();
		}
	}

	/**
	 * Drives the robot to the exit using the Gambler algorithm
	 * 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws exception if robot stopped due to some problem, e.g. lack of energy
	 */
	@Override
	public Direction drive2Exit() throws Exception {	
		//this.getRobot().getMaze().MazeView.driverType = 0;
		//while (this.getRobot().isAtGoal() == false) {
			
		
			
			gatherAvailableMoves();
			Direction randomMove = pickRandomDirection();
			
			rotateAndMove(randomMove);
		//}
		if (this.getRobot().isAtGoal() == true) {
			stepOut();
		}
		return randomMove;
	}
}