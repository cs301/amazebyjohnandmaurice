package edu.wm.cs.cs301.johnandmaurice.falstad;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Direction;

import java.util.Arrays;
import java.lang.Math;
import java.util.Collection;

/**
 * The wizard knows the maze's distance matrix and uses it to find the exit.
 * 
 * @author John Sawin
 * @author Maurice Hayward
 */
public class Wizard extends ManualDriver {

	HashMap<int[], Direction> Direction_Positon = new HashMap<int[], Direction>();
	HashMap<int[], Integer> Position_Distance = new HashMap<int[], Integer>();
	
	/**
	 * Default constructor for the Wizard class, using the standard robot configuration
	 */
	public Wizard() {
		Robot r = new BasicRobot(2500, true, true, true, false, true, false);
		try {
			this.setRobot(r);
		} catch (UnsuitableRobotException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Drives the robot to the exit using the Wizard algorithm
	 * 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws exception if robot stopped due to some problem, e.g. lack of energy
	 */
	@Override
	public synchronized Direction drive2Exit() throws Exception {
		//this.getRobot().getMaze().MazeView.driverType = 1;
		Direction minDirection;


		gatherAvailableMoves();
		gatherAvailableDistances();

		minDirection = getMinDirection();

		//rotateAndMove(minDirection);

		if (this.getRobot().isAtGoal() == true) {
			stepOut();
		}
		return minDirection;
	}

	/**
	 * Determines the direction which will bring us closest to the goal
	 * @return the minimum direction
	 */
	private Direction getMinDirection() {
		int min = Collections.min(Position_Distance.values());
		Direction minDirection = null;
		for (int[] key : Position_Distance.keySet() )
		{
		    if(min == Position_Distance.get(key)){
		    	minDirection = Direction_Positon.get(key);
		    }
		}
		return minDirection;
	}

	/**
	 * Gathers up all the available distances from the possibleMoves list
	 */
	private void gatherAvailableDistances() {
		int length = possibleMoves.size();
		for(int i =0; i < length; i++){
			Direction direction = possibleMoves.get(i);
			int [] Adjposition = this.getRobot().getAdjDirection(direction);
			Direction_Positon.put(Adjposition, direction);
			Position_Distance.put(Adjposition, getDistanceValue(Adjposition));
		}
	}
	
	/**
	 * Gets the value of the distance from a given position
	 * @param position array to specify position
	 * @return distance value
	 */
	private int getDistanceValue(int[] position){
		return this.getRobot().getMaze().mazedists.getDistance(position[0], position[1]);
	}
}