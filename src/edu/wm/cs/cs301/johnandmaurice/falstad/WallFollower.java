package edu.wm.cs.cs301.johnandmaurice.falstad;

import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Direction;

/**
 * Follows the wall on its left hand side.
 * 
 * @author John Sawin
 * @author Maurice Hayward
 */
public class WallFollower extends ManualDriver {
	
	/**
	 * Default constructor for the WallFollower class, using the standard robot configuration
	 */
	public WallFollower() {
		Robot r = new BasicRobot(2500, true, true, true, false, true, false);
		try {
			this.setRobot(r);
		} catch (UnsuitableRobotException e) {
			//e.printStackTrace();
		}
	}

	/**
	 * Drives the robot to the exit using the Wall Follower algorithm
	 * 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws exception if robot stopped due to some problem, e.g. lack of energy
	 */
	@Override
	public Direction drive2Exit() throws Exception {
		//this.getRobot().getMaze().MazeView.driverType = 0;
		//while (this.getRobot().isAtGoal() == false) {
			//System.out.println("is not at goal");
//			if (this.getRobot().getBatteryLevel() <= 0) {
//				return false;
//			}
			
			gatherAvailableMoves();
			
			Direction NextMove;
			
			if(possibleMoves.contains(Direction.LEFT)){NextMove = Direction.LEFT;}
			else{NextMove = possibleMoves.get(0);}
			
			//rotateAndMove(NextMove);
			
			
		
		/*if (this.getRobot().isAtGoal() == true) {
			this.getRobot().getMaze().state = Constants.STATE_FINISH;
			//this.getRobot().getMaze().notifyViewerRedraw();
		}*/
		return NextMove ;
	}
}