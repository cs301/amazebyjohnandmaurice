package edu.wm.cs.cs301.johnandmaurice.falstad;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//import java.awt.event.KeyListener;

import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Direction;
import edu.wm.cs.cs301.johnandmaurice.falstad.Robot.Turn;


public class BasicRobot implements Robot {
	protected Maze maze = new Maze();
	private float batteryLevel;
	
	//test

	public void setCellCount(int cellCount) {
		this.cellCount = cellCount;
	}

	public Cardinal currentCardinal;

	private boolean junctionSensor;
	private boolean roomSensor;
	private boolean forwardDistanceSensor;
	private boolean backwardDistanceSensor;
	private boolean leftDistanceSensor;
	private boolean rightDistanceSensor;
	public List<Direction> possibleMoves = new ArrayList<Direction>();
	protected SingleRandom random;
	int cellCount;

	/**
	 * constructor for BasicRobot, with parameters for possible configurations
	 * 
	 * @param batteryLevel the initial battery level
	 * @param junctionSensor true if exists
	 * @param roomSensor true if exists
	 * @param forwardDistanceSensor true if exists
	 * @param backwardDistanceSensor true if exists
	 * @param leftDistanceSensor true if exists
	 * @param rightDistanceSensor true if exists
	 */
	public BasicRobot (float batteryLevel, boolean junctionSensor, boolean roomSensor, boolean forwardDistanceSensor, 
			boolean backwardDistanceSensor, boolean leftDistanceSensor, boolean rightDistanceSensor) {
		this.batteryLevel = batteryLevel;

		this.junctionSensor = junctionSensor;
		this.roomSensor = roomSensor;
		this.forwardDistanceSensor = forwardDistanceSensor;
		this.backwardDistanceSensor = backwardDistanceSensor;
		this.leftDistanceSensor = leftDistanceSensor;
		this.rightDistanceSensor = rightDistanceSensor;
		random = SingleRandom.getRandom();
		//TestEverything();
	}

	/**
	 * Turn robot on the spot. If robot runs out of energy, it stops and throws an Exception, 
	 * which can be checked by hasStopped() == true and by checking the battery level. 
	 * @param direction to turn to relative to current forward direction 
	 * @throws Exception if the robot stops for lack of energy. 
	 */
	@Override
	public void rotate(Turn turn) throws Exception {

		if (this.batteryLevel <= 0) {
			checkAndNotifyDead();
			throw new Exception();
		}
		int x= getCurrentDirection()[0];
		int y = getCurrentDirection()[1];
		
		switch(turn){
		case RIGHT: rotateRight();
		break;
		case LEFT: rotateLeft();
		break;
		case AROUND: rotateAround();
		break;
		}
		int[] end = getCurrentDirection();
		setCardinalDirection();
	}

	/**
	 * Moves robot forward a given number of steps. A step matches a single cell.
	 * Since a robot may only have a distance sensor in its front.
	 * If the robot runs out of energy somewhere on its way, it stops, 
	 * which can be checked by hasStopped() == true and by checking the battery level. 
	 * If the robot hits an obstacle like a wall, it remains at the position in front 
	 * of the obstacle but hasStopped() == false.
	 * @param distance is the number of cells to move in the robot's current forward direction
	 * @throws Exception if robot hits an obstacle like a wall or border, 
	 * which indicates that current position is not as expected. 
	 * Also thrown if robot runs out of energy. 
	 * @precondition distance >= 0
	 */
	@Override
	public void move(int distance) throws Exception {
		
		int key;
		key = '8';
		setCardinalDirection();
		
		for (int i = 0; i < distance; i++) {
			if (this.getBatteryLevel() <= 0) {
				checkAndNotifyDead();
				throw new Exception("Ran out of energy");
			}
			if(distanceToObstacle(Direction.FORWARD) == 0){
				throw new Exception("Hit obstacle");
			}
			
			if (this.isSteppingOut()) {
				this.maze.state = Constants.STATE_FINISH;
				this.maze.notifyViewerRedraw() ;
			}
			
			this.maze.keyDown(key);
			
			this.setBatteryLevel(this.getBatteryLevel() - 5);
			cellCount = cellCount + 1;
		}
		
	}

	/**
	 * Provides the current position as (x,y) coordinates for the maze cell as an array of length 2 with [x,y].
	 * @postcondition 0 <= x < width, 0 <= y < height of the maze. 
	 * @return array of length 2, x = array[0], y=array[1]
	 * @throws Exception if position is outside of the maze
	 */
	@Override
	public int[] getCurrentPosition() throws Exception {
		int[] currentPosition = new int[2];
		currentPosition[0] = this.maze.px;
		currentPosition[1] = this.maze.py;

		if (this.maze.mazew < currentPosition[0] || this.maze.mazeh < currentPosition[1]) {
			throw new Exception("Position is out of bounds");
		}

		return currentPosition;
	}

	/**
	 * Provides the robot with a reference to the maze it is currently in.
	 * The robot memorizes the maze such that this method is most likely called only once
	 * and for initialization purposes. The maze serves as the main source of information
	 * about the current location, the presence of walls, the reaching of an exit.
	 * @param maze is the current maze
	 * @precondition maze != null, maze refers to a fully operational, configured maze object
	 */
	@Override
	public void setMaze(Maze maze) {
		this.maze = maze;
	}

	/**
	 * Tells if current position is at the goal (the exit). Used to recognize termination of a search.
	 * @return true if robot is at the goal, false otherwise
	 */
	@Override
	public boolean isAtGoal() {
		// Using isatgoalfunction in cells
		int[] currentPosition;
		try {
			currentPosition = getCurrentPosition();
		} catch (Exception e) {
			throw new UnsupportedOperationException("Position is out of bounds");
		}

		int x = currentPosition[0];
		int y = currentPosition[1];

		return maze.getMazecells().isExitPosition(x, y);
	}

	/**
	 * Tells if a sensor can identify the goal in given direction relative to 
	 * the robot's current forward direction from the current position.
	 * @return true if the goal (here: exit of the maze) is visible in a straight line of sight
	 * @throws UnsupportedOperationException if robot has no sensor in this direction
	 */
	@Override
	public boolean canSeeGoal(Direction direction)
			throws UnsupportedOperationException {
		
		if (this.hasDistanceSensor(direction) == false) {
			throw new UnsupportedOperationException("no sensor in that direction");
		}
		
		return this.distanceToObstacle(direction) > this.maze.mazeh;
	}

	/**
	 * Tells if current position is at a junction. 
	 * A junction is a position where there is no wall to the robot's right or left. 
	 * Note that this method is not helpful when the robot is inside a room. 
	 * For most positions inside a room, the robot has no walls to it's right or left
	 * such that the method returns true.
	 * @return true if robot is at a junction, false otherwise
	 * @throws UnsupportedOperationException if not supported by robot
	 */	
	@Override
	public boolean isAtJunction() throws UnsupportedOperationException {
		// checks for junction sensor, throws exception if not found
		if (this.hasJunctionSensor() == false) {
			throw new UnsupportedOperationException("no junction sensor");}
		
//		//Finds the distance to a obstacle at the right and left
//		int right2Obstacle =  this.distanceToObstacle(Direction.RIGHT);
//		int left2Obstacle =  this.distanceToObstacle(Direction.LEFT);
//
//		// if there are no walls on the left and the right of the current position, return true
//		if (right2Obstacle > 0 && left2Obstacle > 0) {return true;}
		
		setCardinalDirection();
		int[] currentPosition;
		try {
			currentPosition = getCurrentPosition();
		} catch (Exception e) {
			throw new UnsupportedOperationException();
		}
	
		int x = currentPosition[0];
		int y = currentPosition[1];
		
		boolean isatJunction = false;
		
		switch(currentCardinal){
		case EAST: isatJunction = canGoNorth(x, y) || canGoSouth(x, y);
			break;
		case NORTH: isatJunction = canGoEast(x, y) || canGoWest(x, y);
			break;
		case SOUTH: isatJunction = canGoEast(x, y) || canGoWest(x, y);
			break;
		case WEST: isatJunction = canGoNorth(x, y) || canGoSouth(x, y);
			break;
		}
		
		return isatJunction;
	}

	/**
	 * Tells if the robot has a junction sensor.
	 */
	@Override
	public boolean hasJunctionSensor() {
		return this.junctionSensor;
	}

	/**
	 * Tells if current position is inside a room. 
	 * @return true if robot is inside a room, false otherwise
	 * @throws UnsupportedOperationException if not supported by robot
	 */	
	@Override
	public boolean isInsideRoom() throws UnsupportedOperationException {
		//Check if there exist a room sensor
		if (this.hasRoomSensor() == false) {
			throw new UnsupportedOperationException("no room sensor");}

		// get Current position
		int[] currentPosition;
		try {
			currentPosition = getCurrentPosition();
		} catch (Exception e) {
			throw new UnsupportedOperationException("OUT OF BOUNDS BRO!!!");
		}

		int x = currentPosition[0];
		int y = currentPosition[1];

		return this.maze.mazecells.isInRoom(x, y);
	}

	/**
	 * Tells if the robot has a room sensor.
	 */
	@Override
	public boolean hasRoomSensor() {
		return this.roomSensor;
	}

	/**
	 * Provides the current direction as (dx,dy) values for the robot as an array of length 2 with [dx,dy].
	 * Note that dx,dy are elements of {-1,0,1} and as in bitmasks masks in Cells.java and dirsx,dirsy in MazeBuilder.java.
	 * 
	 * @return array of length 2, dx = array[0], dy=array[1]
	 */	
	@Override
	public int[] getCurrentDirection() {
		int [] currentDirection = new int[2];
		currentDirection[0] = this.maze.dx;
		currentDirection[1] = this.maze.dy;
		return currentDirection;
	}

	/**
	 * Returns the current battery level.
	 * The robot has a given battery level (energy level) that it draws energy from during operations. 
	 * The particular energy consumption is device dependent such that a call for distance2Obstacle may use less energy than a move forward operation.
	 * If battery level <= 0 then robot stops to function and hasStopped() is true.
	 * @return current battery level, level is > 0 if operational. 
	 */
	@Override
	public float getBatteryLevel() {
		return this.batteryLevel;
	}

	/** 
	 * Sets the current battery level.
	 * The robot has a given battery level (energy level) that it draws energy from during operations. 
	 * The particular energy consumption is device dependent such that a call for distance2Obstacle may use less energy than a move forward operation.
	 * If battery level <= 0 then robot stops to function and hasStopped() is true.
	 * @param level is the current battery level
	 * @precondition level >= 0 
	 */
	@Override
	public void setBatteryLevel(float level) {
		this.batteryLevel = level;
	}

	/**
	 * Gives the energy consumption for a full 360 degree rotation.
	 * Scaling by other degrees approximates the corresponding consumption. 
	 * @return energy for a full rotation
	 */
	@Override
	public float getEnergyForFullRotation() {
		return 12;
	}

	/**
	 * Gives the energy consumption for moving forward for a distance of 1 step.
	 * For simplicity, we assume that this equals the energy necessary 
	 * to move 1 step backwards and that scaling by a larger number of moves is 
	 * approximately the corresponding multiple.
	 * @return energy for a single step forward
	 */
	@Override
	public float getEnergyForStepForward() {
		return 5;
	}

	/**
	 * Tells if the robot has stopped for reasons like lack of energy, hitting an obstacle, etc.
	 * @return true if the robot has stopped, false otherwise
	 */
	@Override
	public boolean hasStopped() {
		if (this.getBatteryLevel() <= 0) {
			return true;
		} else if (this.distanceToObstacle(Direction.FORWARD) == 0) {
			return true;
		}

		return false;
	}

	/**
	 * Tells the distance to an obstacle (a wall or border) for a the robot's current forward direction.
	 * Distance is measured in the number of cells towards that obstacle, 
	 * e.g. 0 if current cell has a wall in this direction
	 * @return number of steps towards obstacle if obstacle is visible 
	 * in a straight line of sight, Integer.MAX_VALUE otherwise
	 * @throws UnsupportedOperationException if not supported by robot
	 * 
	 */
	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
		//checks if there is a sensor in the given direction
		if (this.hasDistanceSensor(direction) == false) {
			throw new UnsupportedOperationException("no sensor in that direction");
		}
		
		//Convert given direction to the exact Cardinal direction
		Cardinal absoluteCardinal = relativeToCardinal(direction);

		//Trys to get CurrentPosition. If OutOFBOUNDS, throws exception
		int[] currentPosition;
		try {
			currentPosition = getCurrentPosition();
		} catch (Exception e) {
			throw new UnsupportedOperationException();
		}
		
		int x = currentPosition[0];
		int y = currentPosition[1];
		
		//Find the distance to Wall is given absolute direction
		int distance = 0;
		switch(absoluteCardinal){
		case NORTH: distance = checkNorthObstacle(x,y);
		break;
		case EAST:distance = checkEastObstacle(x,y);
		break;
		case SOUTH: distance = checkSouthObstacle(x,y);
		break;
		case WEST: distance = checkWestObstacle(x,y);
		break;	
		}
		//Decrement battery level
		this.setBatteryLevel(this.getBatteryLevel() - 1);
		//this.getMaze().MazeView.energy -= 1;
		

		return distance;
	}

	/**
	 * Tells if the robot has a distance sensor for the given direction.
	 */
	@Override
	public boolean hasDistanceSensor(Direction direction) {
		switch(direction){
		case RIGHT: return this.rightDistanceSensor;
		case LEFT: return this.leftDistanceSensor;
		case FORWARD: return this.forwardDistanceSensor;
		case BACKWARD: return this.backwardDistanceSensor;
		}
		return false;
	}
	
	/**
	 * determines if the robot is stepping out of the maze
	 * @return true if is at the goal cell, and can see the goal in the forward direction
	 */
	@Override
	public boolean isSteppingOut() {
		// Using isatgoalfunction in cells

		return (this.isAtGoal() && this.canSeeGoal(Direction.FORWARD));
	}

	/**
	 *Look at Robots Current direction and sets Robots Carndinal direction
	 *
	 */
	private void setCardinalDirection(){
		if (Arrays.equals(Constants.NORTH, getCurrentDirection())){this.currentCardinal = Cardinal.NORTH;}
		if (Arrays.equals(Constants.SOUTH, getCurrentDirection())){this.currentCardinal = Cardinal.SOUTH;}
		if (Arrays.equals(Constants.WEST, getCurrentDirection())){this.currentCardinal = Cardinal.WEST;}
		if (Arrays.equals(Constants.EAST, getCurrentDirection())){this.currentCardinal = Cardinal.EAST;}
	}

	/**
	 *Rotates robot left and depletes robot energy
	 *
	 */
	private void rotateLeft(){
		int key;
		key ='4';
		this.maze.keyDown(key);
		setCardinalDirection();
		this.setBatteryLevel(this.getBatteryLevel() - getEnergyForFullRotation()/4);
		//this.getMaze().MazeView.energy -= 3;
	}

	/**
	 *Rotates robot right and depletes robot energy
	 *
	 */
	private void rotateRight(){
		int key;
		key = '6';
		this.maze.keyDown(key);
		setCardinalDirection();
		this.setBatteryLevel(this.getBatteryLevel() - getEnergyForFullRotation()/4);
		//this.getMaze().MazeView.energy -= 3;
	}

	/**
	 *Rotates robot around and depletes robot energy
	 *
	 */
	private void rotateAround(){
		int key;
		key = '6';
		this.maze.keyDown(key);
		this.maze.keyDown(key);
		setCardinalDirection();
		this.setBatteryLevel(this.getBatteryLevel() - getEnergyForFullRotation()/2);
		//this.getMaze().MazeView.energy -= 6;
	}

	/**
	 *@return the Cardinal direction if the robot looks Left
	 */
	private Cardinal lookLeft(){
		setCardinalDirection();
		
		Cardinal leftCardinal;
		switch(this.currentCardinal){
		case NORTH: leftCardinal = Cardinal.EAST;
		break;
		case EAST: leftCardinal = Cardinal.SOUTH;
		break;
		case SOUTH: leftCardinal = Cardinal.WEST;
		break;
		case WEST: leftCardinal = Cardinal.NORTH;
		break;
		default: leftCardinal = this.currentCardinal;
		}
		return leftCardinal;
	}

	/**
	 *@return the Cardinal direction if the robot looks Right
	 */
	private Cardinal lookRight(){
		setCardinalDirection();

		Cardinal rightCardinal;
		switch(this.currentCardinal){
		case NORTH: rightCardinal= Cardinal.WEST;
		break;
		case EAST: rightCardinal= Cardinal.NORTH;
		break;
		case SOUTH: rightCardinal = Cardinal.EAST;	
		break;
		case WEST: rightCardinal = Cardinal.SOUTH;
		break;
		default: rightCardinal =  this.currentCardinal;

		}
		return rightCardinal;
	}

	/**
	 *@return the Cardinal direction if the robot looks Backwards
	 */
	private Cardinal lookBackward(){
		setCardinalDirection();
		
		Cardinal backCardinal;
		switch(this.currentCardinal){
		case NORTH: backCardinal = Cardinal.SOUTH;	
		break;
		case EAST: backCardinal = Cardinal.WEST;
		break;
		case SOUTH: backCardinal =  Cardinal.NORTH;
		break;
		case WEST: backCardinal = Cardinal.EAST;
		break;
		default: backCardinal = this.currentCardinal;
		}
		return backCardinal;
	}

	/**
	 * @param the direction you would like to look at relative to the robots current direction  
	 *@return the Cardinal direction of given relative direction 
	 */
	public Cardinal relativeToCardinal(Direction direction){
		setCardinalDirection();

		Cardinal relativeCardinal;
		switch(direction){
		case FORWARD: relativeCardinal = currentCardinal;
		break;
		case BACKWARD : relativeCardinal =  lookBackward();
		break;
		case LEFT: relativeCardinal = lookLeft();
		break;
		case RIGHT: relativeCardinal = lookRight();
		break;
		default: relativeCardinal = currentCardinal;

		}
		return relativeCardinal;

	}
	/**
	 * @param x position of robot in the maze 
	 * @param y position of robot in maze
	 *@return boolean if it can go North or Not
	 */
	private boolean canGoNorth(int x , int y){
		return this.maze.mazecells.hasNoWallOnTop(x, y);
	}

	/**
	 * @param x position of robot in the maze 
	 * @param y position of robot in maze
	 *@return distance to an obstacle in the North Direction 
	 */
	private int checkNorthObstacle(int x, int y) {

		int distance = 0;

		//Checks if current positions has a wall until it reach a Wall
		while(canGoNorth(x, y)){
			distance = distance + 1;
			y = y - 1;

			//If Robot has direct line of sight return Interger.Max_Value
			if(y >= this.maze.mazeh || y <= 0){distance = Integer.MAX_VALUE; break;}

		}
		return distance;
	}

	/**
	 * @param x position of robot in the maze 
	 * @param y position of robot in maze
	 *@return boolean if it can go south or Not
	 */
	private boolean canGoSouth(int x , int y){
		return this.maze.mazecells.hasNoWallOnBottom(x, y);
	}
	
	/**
	 * @param x position of robot in the maze 
	 * @param y position of robot in maze
	 *@return distance to an obstacle in the South Direction 
	 */
	private int checkSouthObstacle(int x, int y) {
		int distance = 0;

		//Checks if current positions has a wall until it reach a Wall
		while(canGoSouth(x, y)){
			distance = distance + 1;
			y = y + 1;
			
			//If Robot has direct line of sight return Interger.Max_Value
			if(y >= this.maze.mazeh || y <= 0){distance = Integer.MAX_VALUE; break;}
		}
		return distance;
	}
	
	/**
	 * @param x position of robot in the maze 
	 * @param y position of robot in maze
	 *@return boolean if it can go East or Not
	 */
	private boolean canGoEast(int x , int y){
		return this.maze.mazecells.hasNoWallOnRight(x, y);
	}

	/**
	 * @param x position of robot in the maze 
	 * @param y position of robot in maze
	 *@return distance to an obstacle in the EAST Direction 
	 */
	private int checkEastObstacle(int x, int y) {
		int distance = 0;

		//Checks if current positions has a wall until it reach a Wall
		while(canGoEast(x, y)){
			distance = distance + 1;
			x = x + 1;

			//If Robot has direct line of sight return Interger.Max_Value
			if(x >= this.maze.mazew || x <= 0){distance = Integer.MAX_VALUE; break;}
		}
		return distance;
	}
	
	/**
	 * @param x position of robot in the maze 
	 * @param y position of robot in maze
	 *@return boolean if it can go West or Not
	 */
	private boolean canGoWest(int x , int y){
		return this.maze.mazecells.hasNoWallOnLeft(x, y);
	}

	/**
	 * @param x position of robot in the maze 
	 * @param y position of robot in maze
	 *@return distance to an obstacle in the WEST Direction 
	 */
	private int checkWestObstacle(int x, int y) {
		int distance = 0;

		//Checks if current positions has a wall until it reach a Wall
		while(canGoWest(x, y)){
			distance = distance + 1;
			x = x - 1;

			//If Robot has direct line of sight return Interger.Max_Value
			if(x >= this.maze.mazew || x <= 0){distance = Integer.MAX_VALUE; break;}
		}		
		return distance;
	}
	
	/**
	 * checks to see if the battery has run out, and enters DEAD state if so
	 */
	private void checkAndNotifyDead() {
		if (this.batteryLevel <= 0) {
			this.maze.state = Constants.STATE_DEAD;
			this.maze.notifyViewerRedraw();
		}
	}
	
	/**
	 * Returns current maze
	 * @return the current maze object
	 */
	@Override
	public Maze getMaze() {
		return this.maze;
	}
	
	/**
	 * Gets adjacent direction
	 * @return Adjacent Cell in giving direction
	 */
	@Override
	public int[] getAdjDirection(Direction direction){
		
		int[] currentPosition;
		try {
			currentPosition = getCurrentPosition();
		} catch (Exception e) {
			throw new UnsupportedOperationException("Position is out of bounds");
		}
		int[] AdjPosition = new int[2]; 
		
		int x = currentPosition[0];
		int y = currentPosition[1];
		
		Cardinal AbsDirection = relativeToCardinal(direction);
		switch(AbsDirection){
		case EAST: AdjPosition[0] = x + Constants.EAST[0];
					AdjPosition[1] = y + Constants.EAST[1];
			break;
		case NORTH:AdjPosition[0] = x + Constants.NORTH[0];
		AdjPosition[1] = y + Constants.NORTH[1];
			break;
		case SOUTH:AdjPosition[0] = x + Constants.SOUTH[0];
		AdjPosition[1] = y + Constants.SOUTH[1];
			break;
		case WEST:AdjPosition[0] = x + Constants.WEST[0];
		AdjPosition[1] = y + Constants.WEST[1];
			break;
		}
		
		return AdjPosition;
	}
	
	public int getCellCount() {
		return cellCount;
	}
}