package edu.wm.cs.cs301.johnandmaurice.falstad;

import java.util.ArrayList;

/**
 * This class has the responsibility to create a maze of given dimensions (width, height) together with a solution based on a distance matrix.
 * The Maze class depends on it. The MazeBuilder performs its calculations within its own separate thread such that communication between 
 * Maze and MazeBuilder operates as follows. Maze calls the build() method and provides width and height. Maze has a call back method newMaze that
 * this class calls to communicate a new maze and a BSP root node and a solution.
 * 
 * The maze is built with a randomized version of Aldous Broder's algorithm. 
 * This means a spanning tree is expanded into a set of cells by removing walls from the maze.
 * 
 * This code is refactored code from Maze.java by Paul Falstad, www.falstad.com, Copyright (C) 1998, all rights reserved
 * Paul Falstad granted permission to modify and use code for teaching purposes.
 * @author John Sawin
 * @author Maurice Hayward
 */
public class MazeBuilderAldousBroder extends MazeBuilder implements Runnable {

	int dx = 0;
	int dy = 0;
	
	public MazeBuilderAldousBroder() {
		super();
		System.out.println("MazeBuilderAldousBroder uses AldousBroder's algorithm to generate maze.");
	}
	
	public MazeBuilderAldousBroder(boolean det) {
		super(det);
		System.out.println("MazeBuilderAldousBroder uses AldousBroder's algorithm to generate maze.");
	}

	/**
	 * This method generates pathways into the maze by using Aldous Broder's algorithm. A random starting point 
	 * is selected, and a random direction is chosen for travel until all cells are visited. If the algorithm travels
	 * from the current cell to an unvisited one, the wall between these two cells is removed.
	 */
	@Override
	public void generatePathways() {

		// pick initial position (x,y) at some random position on the maze	
		int x = random.nextIntWithinInterval(0, width-1);
		int y = random.nextIntWithinInterval(0, height - 1);
		int currX = x;
		int currY = y;
		
		// sets the first cell as visited
		this.cells.setCellAsVisited(currX, currY);
		
		// counts the number of remaining cells to visit
		int remaining = width * height -1;
		
		while (true) {
		
			randomDir();
			
			// check if random direction doesn't go out of bounds
			if (isInBound(currX, currY, this.dx, this.dy)) {
				
				// checks if this is the first visit to the next cell
				if(checkFirstVisit(currX + dx, currY + dy)) {
					
					// sets next cell as visited, and deletes wall between the two
					this.cells.setCellAsVisited(currX + dx, currY + dy);
					this.cells.deleteWall(currX, currY, dx, dy);
					
					// decrements counter
					remaining = remaining -1;
					
					// breaks if all cells are visited
					if(remaining == 0){break;}
				}
				
				// sets the current cell to the next cell
				currX = currX + dx;
				currY = currY + dy;
			}			
		}
	}
	
	/**
	 * This method randomizes dx and dy to a valid new direction. Note: not checked to be a possible movement.
	 */
	private void randomDir() {
		int rand = random.nextIntWithinInterval(0, 3);
		
		if (rand == 0) {
			this.dx = 1;
			this.dy = 0;
		}
		else if (rand == 1) {
			this.dx = 0;
			this.dy = 1;
		}
		else if (rand == 2) {
			this.dx = -1;
			this.dy = 0;
		}
		else {
			this.dx = 0;
			this.dy = -1;
		}
	}
	
	/**
	 * Tells if the given position is visited for the first time.
	 * This is true after cells.initialize() and before setCellAsVisited(x,y).
	 * @param x coordinate of cell
	 * @param y coordinate of cell
	 * @return true if (x,y) position is visited for the first time
	 */
	private boolean checkFirstVisit(int x, int y) {
		return this.cells.hasMaskedBitsTrue(x, y, Constants.CW_VISITED);
	}
	
	/**
	 * checks if current cells (x,y) and its neighbor (x+dx,y+dy) are not separated by a border 
	 * @precondition borders limit the outside of the maze area
	 * @param x coordinate of cell
	 * @param y coordinate of cell
	 * @param dx direction x, in { -1, 0, 1} obtained from dirsx[]
	 * @param dy direction y, in { -1, 0, 1} obtained from dirsy[]
	 * @precondition 0 <= x < width, 0 <= y < height
	 * @return true if wall can be taken down, false otherwise
	 */
	private boolean isInBound(int x, int y, int dx, int dy) {
		return (this.cells.hasMaskedBitsTrue(x, y, (this.getBit(dx, dy) << Constants.CW_BOUND_SHIFT)) == false);
	}
	
	/**
	 * encodes (dx,dy) into a bit pattern for right, left, top, bottom direction
	 * @param dx direction x, in { -1, 0, 1} obtained from dirsx[]
	 * @param dy direction y, in { -1, 0, 1} obtained from dirsy[]
	 * @return bit pattern, 0 in case of an error
	 */
	private int getBit(int dx, int dy) {
		int bit = 0;
		switch (dx + dy * 2) {
		case 1:  bit = Constants.CW_RIGHT; break; //  dx=1,  dy=0
		case -1: bit = Constants.CW_LEFT;  break; //  dx=-1, dy=0
		case 2:  bit = Constants.CW_BOT;   break; //  dx=0,  dy=1
		case -2: bit = Constants.CW_TOP;   break; //  dx=0,  dy=-1
		}
		return bit;
	}
}